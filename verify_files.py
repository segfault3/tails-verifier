#!/usr/bin/env python3

import argparse
import collections
import difflib
import logging
import os
import sys
import tempfile
import time
import glob
import json

import mount
import utility
import hash_iuk
import config
import get_iuk_upgraded_versions

usage = """{0} [-h] [-v | --verbose] <partition>
Example: {0} /dev/sdc1""".format(sys.argv[0])


class FileVerifier(object):
    def __init__(self, partition):
        self.partition = partition

        self.iso_hash_file_path = self.get_existing_iso_hash_file_path()
        self.iso_hash_file_present = bool(self.iso_hash_file_path)
        if self.iso_hash_file_present:
            logging.info("Using ISO hash file %r", self.iso_hash_file_path)

        self.has_syslinux_bootloader = os.path.exists(os.path.join(self.partition.mount_point,
                                                                    "syslinux", "ldlinux.sys"))
        self.iuk_versions = get_iuk_upgraded_versions.get_versions(self.partition.path)
        self.is_iuk_upgraded = bool(self.iuk_versions)
        self.iuk_hash_file_paths = self.get_existing_iuk_hash_file_paths()
        self.iuk_hash_files_present = (len(self.iuk_hash_file_paths) == len(self.iuk_versions))
        if self.iuk_hash_files_present:
            for hash_file in self.iuk_hash_file_paths:
                logging.info("Using IUK hash file %r", hash_file)

    def __enter__(self):
        self.device_hash_file_path = tempfile.NamedTemporaryFile(delete=False).name
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.remove(self.device_hash_file_path)

    def get_existing_iso_hash_file_path(self):
        file_path = self.get_iso_hash_file_path()
        if os.path.exists(file_path):
            return file_path
        else:
            return None

    def get_existing_iuk_hash_file_paths(self):
        iuk_hash_paths = list()
        for version in self.iuk_versions:
            matches = glob.glob(os.path.join(config.hash_file_dir, "*%s.iuk%s" % (version, config.hash_file_suffix)))
            if matches and len(matches) > 1:
                raise Exception("Multiple IUK hash files with target version %r found in %r" %(version, config.iuk_dir))
            if matches:
                iuk_hash_paths.append(matches[0])
        return iuk_hash_paths

    def verify_files(self, iso_path: str):
        if not self.iso_hash_file_present:
            self.create_iso_hash_file(iso_path)
        if self.is_iuk_upgraded and not self.iuk_hash_files_present:
            self.create_iuk_hash_files()

        if self.has_syslinux_bootloader:
            self.create_device_hash_file()

        diff = self.compare_hashes()
        if not diff:
            logging.info("Files successfully verified")
            return True
        else:
            logging.warning("Verification of files failed")
            logging.info(diff)
            return False

    def create_iso_hash_file(self, iso_path: str):
        with mount.Mount(iso_path) as iso:
            logging.info("Creating hashes of the files on the Tails ISO Image %r", iso.path)
            self.iso_hash_file_path = self.get_iso_hash_file_path()
            hashes = utility.create_hashes(iso.mount_point)
        logging.debug(hashes)
        self.rename_filenames(hashes)
        # self.copy_syslinux_cfg_hash(hashes)
        # self.replace_access_permissions(hashes)
        hashes = self.sort_hashes(hashes)
        self.write_hashes_to_file(hashes, self.iso_hash_file_path)

    def get_iso_hash_file_path(self):
        iso_file_name = utility.get_iso_file_name(self.partition.path)
        hash_file_name = iso_file_name + config.hash_file_suffix
        return os.path.join(config.hash_file_dir, hash_file_name)

    @staticmethod
    def copy_syslinux_cfg_hash(hashes):
        # The EFI/BOOT/isolinux.cfg seems to be copied to EFI/BOOT/syslinux.cfg during the syslinux installation process
        # XXX: There is no isolinux.cfg anymore, so this is probably obsolete
        isolinux_cfg = os.path.join("isolinux", "isolinux.cfg")
        syslinux_cfg = os.path.join("syslinux", "syslinux.cfg")
        hashes[syslinux_cfg] = hashes[isolinux_cfg]

    @staticmethod
    def replace_access_permissions(hashes):
        # The access permissions differ on the ISO and the installed device
        # On the ISO (and IUK), directories have 555 and regular files have 444
        # On the device all files have 700 except for ldlinux.sys (not present on ISO) and ldlinux.c32, which have 500
        # TODO: Maybe change this during creation of ISOs and IUKs
        for path in hashes:
            hashes[path]['mode'] = hashes[path]['mode'][:-3] + "755"
        hashes["syslinux/ldlinux.c32"]['mode'] = hashes["syslinux/ldlinux.c32"]['mode'][:-3] + "555"

    @staticmethod
    def rename_filenames(hashes):
        import re
        # The directory "isolinux" on the ISO is named "syslinux" on the device
        isolinux_paths = [path for path in hashes if "isolinux" in path]
        for path in isolinux_paths:
            new_path = re.sub(r"^isolinux", r"syslinux", path)
            if new_path != path:
                hashes[new_path] = hashes[path]
                del hashes[path]

        # The "isolinux/isolinux.cfg" is renamed to "syslinux/syslinux.cfg"
        hashes["syslinux/syslinux.cfg"] = hashes["syslinux/isolinux.cfg"]
        del hashes["syslinux/isolinux.cfg"]

    def set_tails_module_hash(self, hashes):
        with tempfile.NamedTemporaryFile(mode="w+") as f:
            f.write("filesystem.squashfs\n")
            for version in self.iuk_versions:
                f.write("%s.squashfs\n" % version)
            f.seek(0)
            logging.debug("Tails.module: %r", f.read())
            hash_ = utility.get_hash_sum(f.name)
        tails_module = os.path.join("live", "Tails.module")
        hashes[tails_module] = collections.OrderedDict()
        hashes[tails_module]["hash"] = hash_
        # hashes[tails_module]["mode"] = "100700"

    def create_iuk_hash_files(self):
        iuk_paths = self.get_iuk_paths()
        for iuk_path in iuk_paths:
            logging.info("Creating hashes of %r", iuk_path)
            with hash_iuk.IUKHasher(iuk_path) as iuk_hasher:
                hashes = iuk_hasher.get_hashes()
                # self.replace_access_permissions(hashes)
                self.set_tails_module_hash(hashes)
                iuk_basename = os.path.basename(iuk_path)
                iuk_hash_file = os.path.join(config.hash_file_dir, iuk_basename + config.hash_file_suffix)
                self.write_hashes_to_file(hashes, iuk_hash_file)
                self.iuk_hash_file_paths.append(iuk_hash_file)

    def get_iuk_paths(self):
        # TODO: Maybe download the IUKs if they are not found
        iuk_paths = list()
        for version in self.iuk_versions:
            matches = glob.glob(os.path.join(config.iuk_dir, "*%s.iuk" % version))
            if not matches:
                raise Exception("No IUK with target version %r found in %r" % (version, config.iuk_dir))
            if len(matches) > 1:
                raise Exception("Multiple IUKs with target version %r found in %r" % (version, config.iuk_dir))
            iuk_paths.append(matches[0])
        return iuk_paths

    def create_device_hash_file(self):
        logging.info("Creating hashes of the files on the device %r", self.partition.path)
        hashes = utility.create_hashes(self.partition.mount_point)
        logging.debug(hashes)
        self.remove_ldlinux_hash(hashes)
        self.remove_tmp_directory(hashes)
        hashes = self.sort_hashes(hashes)
        self.write_hashes_to_file(hashes, self.device_hash_file_path)

    @staticmethod
    def remove_ldlinux_hash(hashes):
        # This is not present on the ISO. It can be verified using verify_ldlinux.py
        ldlinux = os.path.join("syslinux", "ldlinux.sys")
        try:
            del hashes[ldlinux]
        except KeyError:
            logging.error("File 'syslinux/ldlinux.sys' not found. Maybe this device was not "
                          "installed using the Tails Installer?")

    @staticmethod
    def remove_tmp_directory(hashes):
        # This remains as an empty directory after a successful IUK installation
        # TODO: Make the tmp directory being removed by the IUK installer after successful installation
        tmp_dir = os.path.join("tmp")
        if tmp_dir in hashes:
            del hashes[tmp_dir]

    @staticmethod
    def sort_hashes(hashes):
        hashes = collections.OrderedDict(sorted(hashes.items()))
        return hashes

    def compare_hashes(self):
        pseudo_device_hashes = self.read_hashes(self.iso_hash_file_path)
        if self.is_iuk_upgraded:
            pseudo_device_hashes = self.add_iuk_hashes(pseudo_device_hashes)

        device_hashes = self.read_hashes(self.device_hash_file_path)
        pseudo_device_hashes_strings = ["%s %s\n" % (key, pseudo_device_hashes[key]) for key in pseudo_device_hashes]
        device_hashes_strings = ["%s %s\n" % (key, device_hashes[key]) for key in device_hashes]

        diff = difflib.unified_diff(pseudo_device_hashes_strings, device_hashes_strings, "ISO (+ IUKs)", "device", n=0)
        return ''.join(diff)

    def add_iuk_hashes(self, hashes):
        # Add hashes from IUK replacing existing hashes
        for iuk_hash_file in self.iuk_hash_file_paths:
            hashes.update(self.read_hashes(iuk_hash_file))
        hashes = self.sort_hashes(hashes)
        return hashes

    @staticmethod
    def write_hashes_to_file(hashes, file_):
        with open(file_, 'w+') as f:
            json.dump(hashes, f, indent=2, sort_keys=False)

    def read_hashes(self, file_):
        with open(file_, 'r') as f:
            hashes = json.load(f)
        return self.sort_hashes(hashes)


def parse_args():
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument("--verbose", action="store_true")
    parser.add_argument("partition", help="Path to the Tails partition to be verified")
    args = parser.parse_args()
    if not utility.is_block_device(args.partition):
        parser.error("%r doesn't seem to be a block device" % args.partition)
    if utility.is_sdx_drive_device(args.partition):
        parser.error("%r seems to be a drive instead of a partition" % args.partition)
    return args


def init(args):
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    logging.debug("args:", args)


def main():
    args = parse_args()
    init(args)
    date_begin = time.time()
    with mount.Mount(args.partition) as partition, FileVerifier(partition) as verifier:
        verifier.verify_files()
    date_end = time.time()
    logging.info("Verification took %s seconds", int(date_end - date_begin))


if __name__ == "__main__":
    main()
