import os

script_dir = os.path.dirname(os.path.realpath(__file__))
hash_file_dir = os.path.join(script_dir, "hashes")
iuk_dir = os.path.join(script_dir, "iuks")
iso_dir = os.path.join(script_dir, "images")

hash_file_suffix = "_sha256.json"
