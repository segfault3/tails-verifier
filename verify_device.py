#!/usr/bin/env python3

import argparse
import logging
import os
import sys
import time
import verify_files
import verify_ldlinux
import verify_mbr
import verify_vbr
import mount
import utility

usage = """{0} [-h] [-v | --verbose] [--iso <iso>] <drive>
Example: {0} /dev/sdc""".format(sys.argv[0])


def parse_args():
    parser = argparse.ArgumentParser(description="Verify the integrity of a Tails device", usage=usage)
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("--iso", help="Path to the ISO image. If not specified, the ISO will be automatically selected from the images/ directory.")
    parser.add_argument("drive", help="Path to the device to be verified")
    args = parser.parse_args()
    if not utility.is_block_device(args.drive):
        parser.error("%r doesn't seem to be a block device" % args.drive)
    if utility.is_sdx_partition_device(args.drive):
        parser.error("%r seems to be a partition instead of a drive" % args.drive)
    args.partition = utility.get_system_partition(args.drive)
    return args


def init(args):
    format_ = "%(levelname)s:%(message)s"
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, format=format_)
    else:
        logging.basicConfig(level=logging.INFO, format=format_)


def main():
    def verification_failed():
        sys.exit("Verification failed")

    args = parse_args()
    init(args)
    drive_path = args.drive
    partition_path = args.partition
    if args.iso:
        iso_path = args.iso
    else:
        iso_path = utility.get_iso_file_path(partition_path)
    if not os.path.exists(iso_path):
        # TODO: Maybe download the ISO if not found
        raise Exception("No ISO %r found" % iso_path)

    date_begin = time.time()

    logging.info("Checking files...")
    with mount.Mount(partition_path) as partition, \
            verify_files.FileVerifier(partition=partition) as file_verifier:
        success = file_verifier.verify_files(iso_path=iso_path)
    if not success:
        verification_failed()

    logging.info("Checking MBR...")
    success = verify_mbr.verify_mbr(iso_path=iso_path, drive_path=drive_path)
    if not success:
        verification_failed()

    logging.info("Checking VBR...")
    with mount.Mount(iso_path) as iso, \
            mount.CreateAndMountPseudoDevice(iso.syslinux_path) as pseudo_device, \
            verify_vbr.VbrVerifier(iso=iso, pseudo_device=pseudo_device, partition_path=partition_path) as verifier:
        success = verifier.verify_vbr()
    if not success:
        verification_failed()

    logging.info("Checking ldlinux.sys..")
    with mount.Mount(iso_path) as iso, \
            mount.CreateAndMountPseudoDevice(iso.syslinux_path) as pseudo_device, \
            verify_ldlinux.LdlinuxVerifier(iso=iso, pseudo_device=pseudo_device,
                                           partition_path=partition_path) as ldlinux_verifier:
        success = ldlinux_verifier.verify_ldlinux()
    if not success:
        verification_failed()

    logging.info("Tails device successfully verified")
    date_end = time.time()
    logging.info("Verification took %s seconds", int(date_end - date_begin))


if __name__ == "__main__":
    main()
