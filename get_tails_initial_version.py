#!/usr/bin/env python3

import sys
import os
import mount

usage = """usage: {0} <partition>
Example: {0} /dev/sdc1""".format(sys.argv[0])


def get_version(partition_path):
    if not os.geteuid() == 0:
        sys.exit('This script must be run as root')

    with mount.Mount(partition_path) as partition:
        filesystem_path = os.path.join(partition.mount_point, "live", "filesystem.squashfs")
        with mount.Mount(filesystem_path) as filesystem:
            version_path = os.path.join(filesystem.mount_point, "etc", "amnesia", "version")
            with open(version_path) as version_file:
                return read_version(version_file)


def read_version(version_file):
    version = str()
    c = version_file.read(1)
    # Limit maximum size read. This is arbitrary chosen.
    # Longest version string yet seems to be "2.0~beta1" = 9 chars
    while c != ' ' and len(version) < 16:
        version += c
        c = version_file.read(1)
    return version


def parse_args():
    if len(sys.argv) != 2:
        sys.exit(usage)
    return sys.argv[1]


def main():
    partition_path = parse_args()
    print(get_version(partition_path))


if __name__ == "__main__":
    main()
