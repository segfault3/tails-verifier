#!/usr/bin/env python3

import collections
import ctypes
import logging
import re
import struct
import sys
import time

from utility import assert_equal

usage = """Usage: {0} [-v]  <ldlinux.sys>
Example: {0} /lib/live/mount/medium/syslinux/ldlinux.sys""".format(sys.argv[0])

LDLINUX_MAGIC = 0x3eb202fe
LDLINUX_MAGIC_STRING = struct.pack("<I", LDLINUX_MAGIC)


# See syslxmod.c line 197 f. and syslinux/core/diskstart.inc line 118 f.
def calculate_checksum(ldlinux_path):
    patch_area = get_patch_area(ldlinux_path)
    boot_image_len = patch_area['dwords']  # in dwords
    checksum_offset = 0x28
    with open(ldlinux_path, 'rb') as f:
        s = ctypes.c_uint32(LDLINUX_MAGIC)
        for i in range(boot_image_len):
            if i == checksum_offset / 4:  # Don't add the checksum
                f.read(4)
                continue
            s = ctypes.c_uint32(s.value - read(f, 4)[1])
        return s.value


def get_patch_area_with_offsets_and_sizes(ldlinux_path):
    # see libinstaller/syslxint.h for the struct patch_area
    with open(ldlinux_path, 'rb') as f:
        i = f.read().find(LDLINUX_MAGIC_STRING)
        if i == -1:
            raise Exception("file %r doesn't seem to be a valid ldlinux.sys file" % ldlinux_path)
        f.seek(i)
        patch_area = collections.OrderedDict()
        patch_area['magic'] = read(f, 4)
        patch_area['instance'] = read(f, 4)
        patch_area['data_sectors'] = read(f, 2)
        patch_area['adv_sectors'] = read(f, 2)
        patch_area['dwords'] = read(f, 4)
        patch_area['checksum'] = read(f, 4)
        patch_area['maxtransfer'] = read(f, 2)
        patch_area['epaoffset'] = read(f, 2)
        logging.debug("patch_area: %r", patch_area)
        # assert_equal(patch_area['maxtransfer'][1], 127)
        assert_equal(patch_area['magic'][1], LDLINUX_MAGIC)
        return patch_area


def get_patch_area(ldlinux_path):
    patch_area = collections.OrderedDict()
    patch_area_with_offsets_and_sizes = get_patch_area_with_offsets_and_sizes(ldlinux_path)
    for key in patch_area_with_offsets_and_sizes:
        patch_area[key] = patch_area_with_offsets_and_sizes[key][1]
    return patch_area


# See syslinux-6.03+dfsg/core/diskstart.inc lines 68 f.
# Get the extended patch area    
def get_epa_with_offsets_and_sizes(ldlinux_path):
    epa = collections.OrderedDict()
    patch_area = get_patch_area(ldlinux_path)
    with open(ldlinux_path, 'rb') as f:
        f.seek(patch_area['epaoffset'])
        epa['ADVSecPtr'] = read(f, 2)
        epa['CurrentDirPtr'] = read(f, 2)
        epa['CurrentDirLen'] = read(f, 2)
        epa['SubvolPtr'] = read(f, 2)
        epa['SubvolLen'] = read(f, 2)
        epa['SecPtrOffset'] = read(f, 2)
        epa['SecPtrCnt'] = read(f, 2)
        epa['Sect1Ptr0Ptr'] = read(f, 2)
        epa['Sect1Ptr1Ptr'] = read(f, 2)
        epa['RAIDPatchPtr'] = read(f, 2)
        epa['BannerPtr'] = read(f, 2)
    logging.debug("epa: %r" % epa)
    # assert_equal(int(epa['SubvolPtr'][1]) - epa['CurrentDirPtr'][1], 256)
    return epa


def get_epa(ldlinux_path):
    logging.debug("Extracting EPA from %r", ldlinux_path)
    epa = collections.OrderedDict()
    epa_with_offsets_and_sizes = get_epa_with_offsets_and_sizes(ldlinux_path)
    for key in epa_with_offsets_and_sizes:
        epa[key] = epa_with_offsets_and_sizes[key][1]
    return epa


# See syslinux-6.03+dfsg/core/adv.inc line 508 f.
def get_adv_info(ldlinux_path):
    adv_info = collections.OrderedDict()
    epa = get_epa(ldlinux_path)
    with open(ldlinux_path, 'rb') as f:
        f.seek(epa['ADVSecPtr'])
        adv_info['ADVSec0'] = read(f, 8)
        adv_info['ADVSec1'] = read(f, 8)
        adv_info['ADVDrive'] = read(f, 1)
        adv_info['ADVCHSInfo'] = read(f, 1)
    return adv_info


def align(f, i):
    f.read((i - f.tell()) % i)


def read(f, n_bytes):
    offset = f.tell()
    if n_bytes == 1:
        value = struct.unpack("<B", f.read(1))[0]
    elif n_bytes == 2:
        value = struct.unpack("<H", f.read(2))[0]
    elif n_bytes == 4:
        value = struct.unpack("<I", f.read(4))[0]
    elif n_bytes == 8:
        value = struct.unpack("<Q", f.read(8))[0]
    else:
        raise Exception("Invalid value of number n_bytes")
    return offset, value, n_bytes


def get_banner(f):
    banner = b""
    c = f.read(1)
    while c != b'\0':
        banner += c
        c = f.read(1)
    return banner


def get_currentdir(f, v):
    f.seek(v['CurrentDirPtr'][1])
    currentdir = ""
    c = f.read(1)
    while c != '\0':
        currentdir += c
        c = f.read(1)
    return currentdir


def print_ldlinux_sect1(ldlinux_path):
    # See syslinux-6.03+dfsg/core/diskstart.inc lines 54 f.
    patch_area = get_patch_area_with_offsets_and_sizes(ldlinux_path)

    # diskstart.inc lines 68 f.
    epa = get_epa_with_offsets_and_sizes(ldlinux_path)

    with open(ldlinux_path, 'rb') as f:
        # ldlinux.asm line 26 -> head.inc line 35 -> stack.inc line 39 f.
        data16 = collections.OrderedDict()
        f.seek(0x1440)
        align(f, 4)
        data16['BaseStack'] = read(f, 4)
        assert_equal(f.read(2), b'\0\0')

        # ldlinux.asm line 50 -> diskfs.inc line 52 -> diskstart.inc lines 40 f.
        f.seek(0)
        assert_equal(f.read(2), b'\r\n')
        program = f.read(8)
        assert_equal(f.read(1), b' ')
        version = f.read(4)
        assert_equal(f.read(5), b' ' + b'\0\r\n' + b'\x1a')
        assert_equal(f.read(4), b'\0' * 4)
        hexdate = patch_area['instance'][1] ^ LDLINUX_MAGIC

        # diskstart.inc lines 445 f.
        extent_pointers = collections.OrderedDict()
        f.seek(epa['SecPtrOffset'][1])
        align(f, 2)
        for i in range(epa['SecPtrCnt'][1]):
            extent_pointer = read(f, 8)[1]
            length = read(f, 2)[1]
            extent_pointers[i + 1] = (f.tell(), extent_pointer, length)

        f.seek(epa['CurrentDirPtr'][1])
        data16['CurrentDir'] = (f.tell(), f.read(256).strip(b'\0'))
        data16['SubVol'] = (f.tell(), f.read(256).strip(b'\0'))

        # diskstart.inc line 496 -> init.inc line 74
        data16['lzo_data_size'] = read(f, 4)

        # diskstart.inc line 519, 520
        assert_equal(f.tell(), epa['BannerPtr'][1])
        data16['banner'] = (f.tell(), get_banner(f))

        # diskfs.inc line 88 -> com32.inc line 44 f.
        align(f, 4)
        assert_equal(f.read(4), b'\0' * 4)
        assert_equal(f.read(4), b'\x09' + b'\0' * 3)
        assert_equal(f.read(4), b'\0' * 4)
        data16['intcall_entry_point'] = read(f, 4)
        data16['bounce_buffer_address'] = read(f, 4)
        data16['_64k_bounce_buffer'] = read(f, 4)
        data16['farcall_entry_point'] = read(f, 4)
        data16['cfarcall_entry_point'] = read(f, 4)
        data16['end_of_memory_pointer'] = read(f, 4)
        assert_equal(f.read(4), b'\0' * 4)
        data16['protection_mode_functions'] = read(f, 4)

        # diskfs.inc line 116 -> common.inc line 7 -> pm.inc line 175
        align(f, 4)
        data16['protected_mode_esp'] = read(f, 4)
        data16['protected_mode_idt_len'] = read(f, 2)
        data16['protected_mode_idt_ptr'] = read(f, 4)

        # pm.inc line 229
        align(f, 4)
        data16['core_pm_hook'] = read(f, 4)

        # pm.inc line 244
        align(f, 2)
        data16['A20Ptr'] = read(f, 2)

        # pm.inc line 354
        data16['err_a20'] = (f.tell(), f.read(29))
        assert_equal(data16['err_a20'][1], b"\r\nA20 gate not responding!\r\n\0")

        # common.inc line 10 -> adv.inc line 508 f.
        align(f, 8)
        data16['ADVSec0'] = read(f, 8)
        data16['ADVSec1'] = read(f, 8)
        data16['ADVDrive'] = read(f, 1)
        data16['ADVCHSInfo'] = read(f, 1)

        # common.inc line 11 -> timer.inc line 55 f.
        align(f, 4)
        data16['__jiffies'] = read(f, 4)
        data16['__ms_timer'] = read(f, 4)
        data16['__ms_timer_adj'] = read(f, 2)

        # diskfs.inc line 124 f.
        data16['copyright_str'] = (f.tell(), f.read(48))
        assert_equal(re.match(b" Copyright \(C\) 1994-\d\d\d\d H\. Peter Anvin et al\r\n\0", data16['copyright_str'][1]))
        data16['err_bootfailed'] = (f.tell(), f.read(66))
        assert_equal(
            data16['err_bootfailed'][1] == b"\r\nBoot failed: please change disks and press a key to continue.\r\n\0")

        end_of_data16 = f.tell()

    print_values(program, version, hexdate, patch_area, epa, extent_pointers, data16, end_of_data16)
    if patch_area['dwords'][1] != 0:
        checksum = calculate_checksum(ldlinux_path)
        print("Calculated checksum: %x" % checksum)
        assert_equal(checksum, patch_area['checksum'][1])
    else:
        print("Can't calculate checksum because size of ldlinux.sys not set")


def print_values(program, version, hexdate, patch_area, epa, extent_pointers, data16, end_of_data16):
    print("Installation program:", program)
    print("Version:", version)
    print("Hexdate: %r (%x) (%s)" % (hexdate, hexdate, time.asctime(time.gmtime(hexdate))))

    print("\nPatch area:")
    for key in patch_area:
        print("%x %s: %x" % (patch_area[key][0], key, patch_area[key][1]))

    print("\nExtended patch area:")
    for key in epa:
        print("%x %s: %x" % (epa[key][0], key, epa[key][1]))

    print("\nExtent pointers")
    for key in extent_pointers:
        if not extent_pointers[key][1]:
            continue
        print("%x Sector Pointer %s: %08x len: 0x%x" % (
            extent_pointers[key][0], key, extent_pointers[key][1], extent_pointers[key][2]))

    print("\nOther variables:")
    for key in data16:
        offset = data16[key][0]
        value = data16[key][1]
        if isinstance(value, int):
            n_bytes = data16[key][2]
            # value = bytes(("%0" + str(n_bytes * 2) + "X") % value, 'ascii')
            value = ("%0" + str(n_bytes * 2) + "X") % value
        elif isinstance(value, bytes):
            value = value.decode()
        if '\0' in value:
            value = value.strip('\0')
        print("%x %s: %s" % (offset, key, value))
    print("End of .data16 section: %x" % end_of_data16)


def parse_args():
    if "-v" in sys.argv:
        logging.basicConfig(level=logging.DEBUG)
        del (sys.argv[sys.argv.index("-v")])
    else:
        logging.basicConfig(level=logging.INFO)

    if len(sys.argv) < 2:
        sys.exit(usage)

    return sys.argv[1]


def main():
    ldlinux_path = parse_args()
    print_ldlinux_sect1(ldlinux_path)


if __name__ == "__main__":
    try:
        main()
    except:
        sys.stdout.flush()
        raise
