# Tails Verifier #

Verify the integrity of a Tails installation which was installed by the Tails installer.

## Motivation ##
It [has become very clear](http://www.spiegel.de/media/media-35535.pdf#25) that there are people trying to deanonymize Tails users.
The Tails folks (and indirectly the Tor and Debian folks) put a lot of effort into making Tails as secure as possible. But there are other ways for adversaries to deanonymize Tails users than exploiting Tails itself. Below are two attack scenarios which the Tails Verifier can protect against (to some extent, see [below](#Limitations) for Limitations)

### Attack scenarios ###
#### Infect users' day-to-day OS ####
It is possible for an adversary on the ISP level to detect that a Tails user is [using Tails](https://tails.boum.org/doc/about/fingerprint/index.en.html#footer). This makes it possible to target the Tails users' day-to-day operating system, which is probably a lot easier to attack. If this OS is successfully infected and the Tails user then plugs in his Tails device while the infected OS is running, it is trivial to modify the Tails installation in a way that will leak the user's IP address and everything he does within Tails. 

While Tails [warns](https://tails.boum.org/doc/about/warning/index.en.html#index2h1) users against plugging their Tails device into untrusted operating systems, it is most likely that everyone who uses Tails non-exclusively on a machine for a while will eventually plug it in accidentally while another OS is still running. It is also possible to write the infection routine into the bootloader, so it would be sufficient to plug in Tails while the bootloader is running to infect it.

#### Evil Maid Attack ####

If an adversary is able to gain temporary physical access to a Tails device, for example if you leave it in your hotel room or if authorities temporarily seize your stuff, he will also be able to manipulate it.

The Tails Verifier enables users to detect these attacks by checking the integrity of the Tails installation.

## Components ##

The Tails Verifier consists of four separate scripts, each of which verifies one part of the Tails installation, and a script which executes them all in a row.

### verify_device.py ###

This completely verifies a Tails device using the scripts ```verify_files.py```, ```verify_mbr.py```, ```verify_vbr.py``` and ```verify_ldlinux.py```. 

It automatically determines the Tails version of the device and expects the corresponding ISO image to be found in the ```images``` directory.
#### Usage example ####

```
verify_device.py /dev/sdc
```

### verify_files.py ###

Creates hash sums of all the files on a Tails device and all the files on the corresponding Tails ISO image and [Incremental Upgrade Kits](https://tails.boum.org/contribute/design/incremental_upgrades/) (IUKs), if the device was upgraded using IUKs. After making some adjustments which correspond to changes caused by the Tails installation process, it compares these hash sums and outputs whether they match.
It also verifies the file modes, which include the access permissions.

Note that if the Tails device was upgraded using IUKs, this will use the ISO image of the initial Tails version, before the IUK upgrades. It automatically determines the initial Tails version and expects the corresponding ISO image to be found in the ```images``` directory and the IUKs in the ```iuks``` directory.

The hash sums of the ISO image and the IUKs will be written to files in the ```hashes``` directory. If these hash sum files are present from a previous execution, the hash sum calculation of the ISO and the IUKs will be skipped and the hash sums from the files will be used instead.

#### Usage example ####

```
verify_files.py /dev/sdc1
```

### verify_mbr.py ###

Copies the master boot record of the Tails device and compares it with the one stored as a file on the Tails ISO image (in ```utils/mbr/mbr.bin```).

#### Usage example ####

```
verify_mbr.py tails-i386-1.8.2.iso /dev/sdc
```


### verify_vbr.py ###

Verifies the FAT32 boot sector or volume boot record of the Tails device. This is not present on the Tails ISO image, it is created by the Tails installer, more precisely by the [SYSLINUX](www.syslinux.org/wiki/index.php/SYSLINUX) installer executed by the Tails installer. 

In order to verify the boot sector, a pseudo device is created, FAT32 formatted and syslinux is installed on it, which creates a FAT32 boot sector on it. 

There are some values in the boot sector which are specific to the device it was created on, including the device's geometry, the volume ID and the logical block address of the first sector of a file named ```ldlinux.sys```. The first sector of ```ldlinux.sys``` contains the next stage of the boot code, which is executed after the code in the boot sector is executed. 

These values which depend on the specific device are read from the device's boot sector and patched in the newly created boot sector. Then hash sums of both boot sectors are created and compared.

Note that this does not check if the address at which the boot code continues its execution is actually the address of the ```ldlinux.sys``` file. This is verified by the [verify_ldlinux_sys.py](verify_ldlinux_sys.py) script.

#### Usage example ####

```
verify_vbr.py tails-i386-1.8.2.iso /dev/sdc1
```


### verify_ldlinux.py ###

Verifies the syslinux code in ```ldlinux.sys```. This file is not present on the Tails ISO image, it is also created by the syslinux installer during the installation of Tails.

In order to verify ```ldlinux.sys```, the same procedure as with the verification of the [FAT32 boot sector](verify_vbr.py) is used: A pseudo device is created, FAT32 formatted and syslinux is installed on it, which creates the ldlinux.sys.

There are again some values which depend on the device, which are logical block addresses to other sectors of ```ldlinux.sys``` and a checksum. To make sure the addresses are actually inside the ```ldlinux.sys```, the FAT of the device is parsed and the logical block addresses of the sectors of the device's ```ldlinux.sys``` are patched in the newly created ```ldlinux.sys```. Then the checksum of the ```ldlinux.sys``` is calculated and also patched. Then hash sums of both files are created and compared.

The ```ldlinux.sys``` of the device is obtained via ```dd``` using the logical block address found in the FAT32 boot sector. This ensures that the boot code of the boot sector actually continues execution with the ```ldlinux.sys```.

#### Usage example ####

```
verify_ldlinux.py tails-i386-1.8.2.iso /dev/sdc1
```


### Limitations ###

 - The Tails Verifier can only verify Tails devices created with the Tails Installer. It does not work for Tails devices created by copying the ISO image on the device. 
 
 - The Tails Verifier does not verify the integrity of your BIOS and firmware. It is still possible to attack a verified Tails if these are compromised.
 
 - The Tails Verifier does not verify the files on the Persistent partition. They could be manipulated and compromise your system every time you execute them. 
 
 - There might (and probably will) be things that I didn't think about and which allow an adversary who analyzes the code of the Tails Verifier to find loopholes. So it might be possible to manipulate a Tails device in a way that the Tails Verifier will not detect.
 
 - There might be publicly unknown vulnerabilities in either the Linux kernel, the services used to access the device, the Tails Verifier itself or the python functions or underlying code used by the Tails Verifier. This could allow an attacker to manipulate a Tails device in a way that it compromises the system used for verification.
 
 - Currently root privileges are required to execute the Tails Verifier. This could be worked around, but I currently refrain from spending more work on this and rather try to simplify the whole verification by changing the Tails installation process (see below).

### Regarding maintainability ###
 
 - In a few cases the code extracts values from fixed offsets from the VBR. These offsets might change between Syslinux versions. So Syslinux updates might break the Tails Verifier.
 
 - Very dependent on file names and the installation and upgrade behaviour of Tails
 
 - Verification could be a lot easier, safer and more stable if the complete Tails installation could be performed by just copying the ISO image on the device. The way I see it, it would also simplify the verification a lot if only GRUB was used as a bootloader instead of Syslinux, because GRUB's bootcode doesn't seem to need patching and is already available as a file on the ISO image.
