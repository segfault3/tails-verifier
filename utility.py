import hashlib
import logging
import os
import stat
import subprocess
import tempfile
import collections
import time

import config
import get_tails_initial_version


def execute_command(*args, ignore_return_code=False):
    command_str = ' '.join(args)
    logging.debug("Executing command: %r", command_str)
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    if p.returncode != 0 and not ignore_return_code:
        logging.warning("stdout: %s\nstderr: %s", out.decode('ascii', 'replace'), err.decode('ascii', 'replace'))
        raise subprocess.CalledProcessError(cmd=command_str, returncode=p.returncode)
    return out, err


def create_hashes(root_dir):
    date_begin = time.time()
    hashes = collections.OrderedDict()
    root_dir = os.path.normpath(root_dir)
    for current_dir, _, files in os.walk(root_dir):
        relative_dir_path = current_dir[len(root_dir) + 1:]
        logging.info("Hashing files in directory: %r", relative_dir_path)
        # hashes[relative_dir_path] = {"mode": get_mode(current_dir)}
        for file_ in files:
            path = os.path.join(current_dir, file_)
            relative_path = os.path.join(relative_dir_path, file_)
            dict_ = collections.OrderedDict()
            dict_["hash"] = get_hash_sum(path)
            # dict_["mode"] = get_mode(path)
            hashes[relative_path] = dict_
    logging.info("Creating hashes took %r seconds", time.time() - date_begin)
    return hashes


def get_system_partition(drive):
    if is_loop_device(drive):
        partition = drive + "p1"
    elif os.path.basename(drive).startswith("mmcblk"):
        partition = drive + "p1"
    elif os.path.basename(drive).startswith("sd"):
        partition = drive + "1"
    else:
        raise Exception("Unsupported device %r" % drive)
    return partition


def get_hash_sum(file_path, size=-1):
    # TODO: We don't necessarily need collision resistance, so we could go with MD5 or SHA1
    # TODO: Test how big the speed gain of those two would be
    with open(file_path, 'rb') as f:
        hash_sum = hashlib.sha256(f.read(size)).hexdigest()
    return hash_sum


def get_mode(file_):
    return "%o" % (os.stat(file_)[stat.ST_MODE])


def get_vbr(device_path):
    logging.debug("Extracting VBR from %r", device_path)
    out_path = tempfile.mkstemp()[1]
    execute_command("dd", "if=%s" % device_path, "of=%s" % out_path, "bs=512", "count=1")
    return out_path


def get_iso_file_name(partition_path):
        initial_version = get_tails_initial_version.get_version(partition_path)
        return "tails-amd64-%s.iso" % initial_version


def get_iso_file_path(partition_path):
        iso_file_name = get_iso_file_name(partition_path)
        return os.path.join(config.iso_dir, iso_file_name)


def is_block_device(device):
    return stat.S_ISBLK(os.stat(device).st_mode)


def is_sdx_drive_device(device):
    device_stat = os.stat(device)
    return os.major(device_stat.st_rdev) == 8 and os.minor(device_stat.st_rdev) % 16 == 0


def is_sdx_partition_device(device):
    device_stat = os.stat(device)
    return os.major(device_stat.st_rdev) == 8 and not os.minor(device_stat.st_rdev) % 16 == 0


def is_loop_device(device):
    return os.major(os.stat(device).st_rdev) == 7

def assert_equal(a, b):
    if a != b:
        raise Exception("%r does not equal %r" %(a, b))