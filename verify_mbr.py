#!/usr/bin/env python3

import logging
import os
import sys
import tempfile

import mount
import utility

usage = """Usage: {0} [-v] <iso> <drive>
Example: {0} ./iso/tails-i386-1.5.1.iso /dev/sdc""".format(sys.argv[0])


class Mbr(object):
    def __init__(self, drive, size):
        self.drive = drive
        self.size = size
        self.path = tempfile.mkstemp()[1]

    def __enter__(self):
        self.get_mbr()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.remove(self.path)

    def get_mbr(self):
        logging.debug("Extracting MBR from %r", self.drive)
        utility.execute_command("dd", "if=%s" % self.drive, "of=%s" % self.path, "bs=1", "count=%s" % self.size)


def verify_mbr(iso_path, drive_path):
    iso_mbr_hash, mbr_size = get_iso_mbr_hash_and_size(iso_path)
    dev_mbr_hash = get_device_mbr_hash(drive_path, mbr_size)
    if dev_mbr_hash == iso_mbr_hash:
        logging.info("MBR successfully verified")
        return True
    else:
        logging.warning("MBR verification failed")
        return False


def get_iso_mbr_hash_and_size(iso_path):
    with mount.Mount(iso_path) as iso:
        iso_mbr = os.path.join(iso.mount_point, "utils", "mbr", "mbr.bin")
        iso_mbr_hash = utility.get_hash_sum(iso_mbr)
        mbr_size = os.path.getsize(iso_mbr)
    return iso_mbr_hash, mbr_size


def get_device_mbr_hash(device_path, mbr_size):
    with Mbr(device_path, mbr_size) as dev_mbr:
        return utility.get_hash_sum(dev_mbr.path)


def parse_args():
    if len(sys.argv) < 3:
        sys.exit(usage)
    if "-v" in sys.argv:
        logging.basicConfig(level=logging.DEBUG)
        del (sys.argv[sys.argv.index("-v")])
    else:
        logging.basicConfig(level=logging.INFO)
    return sys.argv[1:3]


def main():
    iso, drive = parse_args()
    verify_mbr(iso, drive)


if __name__ == "__main__":
    main()
