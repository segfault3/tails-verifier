#!/usr/bin/env python3

import argparse
import os
import sys
import logging
import time

import utility

usage = """{0} [-h] [-v | --verbose] <drive>
Example: {0} /dev/sdc""".format(sys.argv[0])


def parse_args():
    parser = argparse.ArgumentParser(description="Verify the integrity of a Tails device", usage=usage)
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("drive", help="Path to the device to be verified")
    args = parser.parse_args()
    if not utility.is_block_device(args.drive):
        parser.error("%r doesn't seem to be a block device" % args.drive)
    if utility.is_sdx_partition_device(args.drive):
        parser.error("%r seems to be a partition instead of a drive" % args.drive)
    args.partition = utility.get_system_partition(args.drive)
    return args


def init(args):
    format_ = "%(levelname)s:%(message)s"
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, format=format_)
    else:
        logging.basicConfig(level=logging.INFO, format=format_)


def main():
    def verification_failed():
        sys.exit("Verification failed")

    args = parse_args()
    init(args)
    drive_path = args.drive
    partition_path = args.partition
    iso_path = utility.get_iso_file_path(partition_path)
    if not os.path.exists(iso_path):
        raise Exception("No ISO %r found" % iso_path)
    iso_size = os.path.getsize(iso_path)

    date_begin = time.time()

    logging.debug("Creating ISO hash")
    iso_hash = utility.get_hash_sum(iso_path)

    logging.debug("Creating device hash")
    device_hash = utility.get_hash_sum(drive_path, iso_size)

    if iso_hash != device_hash:
        verification_failed()

    logging.info("Tails device successfully verified")
    date_end = time.time()
    logging.info("Verification took %s seconds", int(date_end - date_begin))


if __name__ == "__main__":
    main()
