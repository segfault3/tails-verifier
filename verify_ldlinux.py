#!/usr/bin/env python3

import logging
import os
import argparse
import struct
import sys
import tempfile
import mount
import parse_fat
import parse_ldlinux
import parse_vbr
import utility

usage = """Usage: {0} [-v] <iso> <partition>
Example: {0} ./iso/tails-i386-1.5.1.iso /dev/sdc1""".format(sys.argv[0])


class LdlinuxVerifier(object):
    def __init__(self, iso, pseudo_device, partition_path):
        logging.debug("Initializing ldlinux.sys verifier")
        self.iso = iso
        self.partition_path = partition_path
        self.pseudo_device = pseudo_device
        self._sector_size = None
        self._adv_sector_pointers = None
        self._extent_pointers = None
        self._extent_pointer_lengths = None

    def __enter__(self):
        self.pseudo_ldlinux = os.path.join(self.pseudo_device.mount_point, "syslinux", "ldlinux.sys")
        self.device_vbr = utility.get_vbr(self.partition_path)

        pseudo_device_epa = parse_ldlinux.get_epa(self.pseudo_ldlinux)
        self.extent_pointer_offset = pseudo_device_epa['SecPtrOffset']
        self.adv_sector_pointer_offset = pseudo_device_epa['ADVSecPtr']

        self.device_ldlinux = self.get_device_ldlinux()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.remove(self.device_ldlinux)
        os.remove(self.device_vbr)

    def get_device_ldlinux(self):
        logging.debug("Extracting ldlinux.sys from %r", self.partition_path)
        adv_sector_pointers, extent_pointers, lengths = self.get_sector_pointers()
        size = os.path.getsize(self.pseudo_ldlinux)
        path = tempfile.mkstemp()[1]
        self.copy_device_ldlinux_first_sector(path)
        for i, extent_pointer in enumerate(extent_pointers):
            self.append_ldlinux_sectors(extent_pointer, lengths[i], path)
        for adv_sector_pointer in adv_sector_pointers:
            self.append_ldlinux_sectors(adv_sector_pointer, 1, path)
        if os.path.getsize(path) > size:
            logging.debug("Truncating %r to %r", path, size)
            with open(path, 'r+') as f:
                f.truncate(size)
        return path

    def get_sector_pointers(self):
        if not (self._adv_sector_pointers and self._extent_pointers and self._extent_pointer_lengths):
            self._adv_sector_pointers, self._extent_pointers, self._extent_pointer_lengths = \
                self.extract_sector_pointers()
        return self._adv_sector_pointers, self._extent_pointers, self._extent_pointer_lengths

    def extract_sector_pointers(self):
        patch_area = parse_ldlinux.get_patch_area(self.pseudo_ldlinux)
        num_data_sectors = patch_area["data_sectors"]
        num_adv_sectors = patch_area["adv_sectors"]
        with parse_fat.FatParser(self.partition_path) as fat_parser:
            extent_pointers, lengths = fat_parser.get_ldlinux_extent_pointers(num_data_sectors, num_adv_sectors)
            adv_sector_pointers = fat_parser.get_ldlinux_adv_sector_pointers(num_data_sectors, num_adv_sectors)
        return adv_sector_pointers, extent_pointers, lengths

    def copy_device_ldlinux_first_sector(self, out_file_path):
        ldlinux_first_sector_lba = parse_vbr.get_ldlinux_first_sector_lba(self.device_vbr)
        sector_size = self.get_sector_size()
        utility.execute_command("dd", "if=%s" % self.partition_path,
                                "of=%s" % out_file_path,
                                "bs=%s" % sector_size,
                                "skip=%s" % ldlinux_first_sector_lba,
                                "count=1")

    def append_ldlinux_sectors(self, pointer, length, out_file_path):
        sector_size = self.get_sector_size()
        utility.execute_command("dd",
                                "if=%s" % self.partition_path,
                                "of=%s" % out_file_path,
                                "conv=notrunc",
                                "oflag=append",
                                "bs=%s" % sector_size,
                                "skip=%s" % pointer,
                                "count=%s" % length)

    def get_sector_size(self):
        if not self._sector_size:
            fat32_boot_sector = parse_vbr.get_fat32_boot_sector(self.device_vbr)
            self._sector_size = fat32_boot_sector["BytesPerSec"]
        return self._sector_size

    def verify_ldlinux(self):
        self.patch_tmp_ldlinux()

        hash_tmp = utility.get_hash_sum(self.pseudo_ldlinux)
        hash_device = utility.get_hash_sum(self.device_ldlinux)
        if hash_tmp == hash_device:
            logging.info("ldlinux.sys successfully verified")
            return True
        else:
            logging.warning("Verification of ldlinux.sys failed")
            self.parse_files_and_print_diff(self.pseudo_ldlinux, self.device_ldlinux)
            return False

    # See syslinux-6.03+dfsg/libinstaller/syslxmod lines 96 f. for the
    # syslinux_patch function of the syslinux installer
    def patch_tmp_ldlinux(self):
        logging.debug("Patching %r", self.pseudo_ldlinux)
        adv_sector_pointers, extent_pointers, lengths = self.get_sector_pointers()

        self.patch_extent_pointers(extent_pointers, lengths)
        self.patch_adv_sector_pointers(adv_sector_pointers)
        self.patch_checksum()

    def patch_extent_pointers(self, extent_pointers, lengths):
        with open(self.pseudo_ldlinux, 'r+b') as f:
            f.seek(self.extent_pointer_offset)
            for i, pointer in enumerate(extent_pointers):
                logging.debug("Setting extent pointer at %x to %08x, len: 0x%x",
                              self.extent_pointer_offset + 8 * i, pointer, lengths[i])
                pointer_str = struct.pack("<Q", pointer)
                length_str = struct.pack("<H", lengths[i])
                f.write(pointer_str)
                f.write(length_str)

    def patch_adv_sector_pointers(self, adv_sector_pointer):
        with open(self.pseudo_ldlinux, 'r+b') as f:
            f.seek(self.adv_sector_pointer_offset)
            f.write(struct.pack("<Q", adv_sector_pointer[0]))
            f.write(struct.pack("<Q", adv_sector_pointer[1]))

    def patch_checksum(self):
        checksum = parse_ldlinux.calculate_checksum(self.pseudo_ldlinux)
        with open(self.pseudo_ldlinux, 'r+b') as f:
            f.seek(0x28)
            f.write(struct.pack("<I", checksum))

    def parse_files_and_print_diff(self, file1, file2):
        import difflib
        out1, out2 = self.parse_files(file1, file2)
        diff = difflib.unified_diff(out1.read().splitlines(1), out2.read().splitlines(1))
        print(''.join(diff))

    @staticmethod
    def parse_files(file1, file2):
        import io
        orig_stdout = sys.stdout
        out1 = io.StringIO()
        sys.stdout = out1
        parse_ldlinux.print_ldlinux_sect1(file1)
        out2 = io.StringIO()
        sys.stdout = out2
        parse_ldlinux.print_ldlinux_sect1(file2)
        sys.stdout = orig_stdout
        out1.seek(0)
        out2.seek(0)
        return out1, out2


def parse_args():
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument("--verbose", action="store_true")
    parser.add_argument("iso", help="Path to the Tails ISO image")
    parser.add_argument("partition", help="Path to the Tails partition to be verified")
    args = parser.parse_args()
    if not utility.is_block_device(args.partition):
        parser.error("%r doesn't seem to be a block device" % args.partition)
    if utility.is_sdx_drive_device(args.partition):
        parser.error("%r seems to be a drive instead of a partition" % args.partition)
    return args


def init(args):
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    logging.debug("args: %s", args)


def main():
    args = parse_args()
    init(args)
    with mount.Mount(args.iso) as iso, \
            mount.CreateAndMountPseudoDevice(iso.syslinux_path) as pseudo_device, \
            LdlinuxVerifier(iso=iso, pseudo_device=pseudo_device, partition_path=args.partition) as ldlinux_verifier:
        ldlinux_verifier.verify_ldlinux()


if __name__ == '__main__':
    main()
