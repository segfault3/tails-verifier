#!/usr/bin/env python3

import collections
import logging
import struct
import sys

from utility import assert_equal

usage = "Usage: %s [-v] <VBR>" % sys.argv[0]


def read(f, n_bytes):
    offset = f.tell()
    if n_bytes == 1:
        value = struct.unpack("<B", f.read(1))[0]
    elif n_bytes == 2:
        value = struct.unpack("<H", f.read(2))[0]
    elif n_bytes == 4:
        value = struct.unpack("<I", f.read(4))[0]
    elif n_bytes == 8:
        value = struct.unpack("<Q", f.read(8))[0]
    else:
        raise Exception("Invalid value of number n_bytes")
    return offset, value, n_bytes


def get_ldlinux_first_sector_lba(vbr_path):
    with open(vbr_path, 'rb') as f:
        # Check whether VBR was created by syslinux
        f.seek(0x3)
        assert_equal(f.read(8), b"SYSLINUX")

        # Read LBA of the first sector
        # TODO: I think this offset might change between syslinux versions
        f.seek(0x11A)
        first_sector_lba = read(f, 4)[1]

        # Check whether the upper 4 bytes of the 8 byte LBA are all 0 They must be all 0,
        # (they should be, because FAT32 uses 4 Byte LBAs)
        f.seek(0x120)
        assert_equal(read(f, 4)[1], 0)

    logging.debug("First sector of ldlinux.sys: %08x", first_sector_lba)
    return first_sector_lba


def print_fat32_boot_sector(vbr_path):
    bs = get_boot_sector_with_offset_and_sizes(vbr_path)
    for key in bs:
        offset = bs[key][0]
        value = bs[key][1]
        if isinstance(value, int):
            n_bytes = bs[key][2]
            value = ("%0" + str(n_bytes * 2) + "X") % value
        if key == 'Code':
            step = 0x4
            print("%x %s:" % (offset, key))
            for i in range(0, len(value), step):
                print('\t %3X %s ' % (offset + i, value[i:i + step]))
        else:
            print("%x %s: %r" % (offset, key, value))


def get_fat32_boot_sector(vbr_path):
    bs = collections.OrderedDict()
    bs_with_offsets_and_sizes = get_boot_sector_with_offset_and_sizes(vbr_path)
    for key in bs_with_offsets_and_sizes:
        bs[key] = bs_with_offsets_and_sizes[key][1]
    return bs


# See libinstaller/syslxint.h lines 222 f.
def get_boot_sector_with_offset_and_sizes(vbr_path):
    logging.debug("Parsing FAT32 boot sector")
    bs = collections.OrderedDict()
    with open(vbr_path, 'rb') as f:
        # set in libinstaller/fs.c line 34 f.
        bs["Jump"] = (f.tell(), f.read(3), 3)

        # set in core/diskboot.inc line 59 f.
        bs["OemName"] = (f.tell(), f.read(8), 8)

        # set in extlinux/main.c line 290f.
        bs["BytesPerSec"] = read(f, 2)

        # TODO: where are these set?
        bs["SecPerClust"] = read(f, 1)
        bs["ResSectors"] = read(f, 2)
        bs["FATs"] = read(f, 1)
        bs["RootDirEnts"] = read(f, 2)

        # set in extlinux/main.c line 290f.
        bs["Sectors"] = read(f, 2)

        # TODO: where are these set?
        bs["Media"] = read(f, 1)
        bs["FATsecs"] = read(f, 2)

        # set in extlinux/main.c line 290f.
        bs["SecPerTrack"] = read(f, 2)
        bs["Heads"] = read(f, 2)
        bs["HiddenSecs"] = read(f, 4)
        bs["HugeSecs"] = read(f, 4)

        # TODO: where are these set?
        bs["FATSz32"] = read(f, 4)
        bs["ExtFlags"] = read(f, 2)
        bs["FSVer"] = read(f, 2)
        bs["RootClus"] = read(f, 4)
        bs["FSInfo"] = read(f, 2)
        bs["BkBootSec"] = read(f, 2)
        bs["Reserved0"] = (f.tell(), f.read(12), 12)
        bs["DriveNumber"] = read(f, 1)
        bs["Reserved1"] = (f.tell(), f.read(1))
        bs["BootSignature"] = read(f, 1)
        bs["VolumeID"] = read(f, 4)
        bs["VolumeLabel"] = (f.tell(), f.read(11), 11)
        bs["FileSysType"] = (f.tell(), f.read(8), 8)

        # set in libinstaller/fs.c line 34 f.
        # patched in libinstaller/syslxmod.c line 131 f. (sect1ptr0, sect1ptr1 and raidpatch)
        bs["Code"] = (f.tell(), f.read(414), 414)

        # TODO: where are these set?
        bs["Magic"] = read(f, 4)
        bs["ForwardPtr"] = read(f, 2)
        bs["Signature"] = read(f, 2)

    logging.debug("FAT32 boot sector:")
    for e in bs:
        logging.debug("%x %s: %r" % (bs[e][0], e, bs[e][1]))

    return bs


def parse_args():
    if len(sys.argv) < 2:
        sys.exit(usage)

    if "-v" in sys.argv:
        logging.basicConfig(level=logging.DEBUG)
        del (sys.argv[sys.argv.index("-v")])
    else:
        logging.basicConfig(level=logging.INFO)

    return sys.argv[1]


def main():
    vbr_path = parse_args()
    print_fat32_boot_sector(vbr_path)


if __name__ == '__main__':
    main()
