import tempfile
import tarfile
import re
import shutil
import collections

import utility

boot_tar_name = "boot.tar.bz2"
system_tar_name = "system.tar"

class IUKHasher(object):
    def __init__(self, iuk_path):
        self.path = iuk_path

    def __enter__(self):
        self.system_extract_path = tempfile.mkdtemp()
        self.boot_extract_path = tempfile.mkdtemp()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        shutil.rmtree(self.system_extract_path)
        shutil.rmtree(self.boot_extract_path)

    def get_versions(self):
        match = re.match("Tails_i386_(.*)_to_(.*).iuk", self.path)
        return match.group(1), match.group(2)

    def get_hashes(self):
        hashes = collections.OrderedDict()
        with tarfile.open(name=self.path, mode="r") as iuk_tar:
            system_tar_hashes = self.get_system_tar_hashes(iuk_tar)
            boot_tar_hashes = self.get_boot_tar_hashes(iuk_tar)
        hashes.update(system_tar_hashes)
        hashes.update(boot_tar_hashes)
        return hashes

    def get_system_tar_hashes(self, iuk_tar):
        with iuk_tar.extractfile(system_tar_name) as system_tar_fileobj:
            with tarfile.open(fileobj=system_tar_fileobj, mode="r") as system_tar:
                for member in system_tar.getmembers():
                    system_tar.extract(member, path=self.system_extract_path)
        return utility.create_hashes(self.system_extract_path)

    def get_boot_tar_hashes(self, iuk_tar):
        with iuk_tar.extractfile(boot_tar_name) as boot_tar_fileobj:
            with tarfile.open(fileobj=boot_tar_fileobj, mode="r") as boot_tar:
                for member in boot_tar.getmembers():
                    boot_tar.extract(member, path=self.boot_extract_path)
        return utility.create_hashes(self.boot_extract_path)


if __name__ == "__main__":
    import sys
    iuk_path = sys.argv[1]
    with IUKHasher(iuk_path) as iuk_hasher:
        print(iuk_hasher.get_hashes())