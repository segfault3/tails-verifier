import logging
import os
import subprocess
import tempfile

import utility


class Mount(object):
    def __init__(self, path):
        logging.debug("Mounting %r", path)
        self.path = path

    @staticmethod
    def mount(path, mount_point):
        utility.execute_command("mount", "-o", "ro", path, mount_point)

    def __enter__(self):
        self.vbr = utility.get_vbr(self.path)
        self.mount_point = tempfile.mkdtemp()
        self.mount(self.path, self.mount_point)
        self.syslinux_path = os.path.join(self.mount_point, "utils/linux/syslinux")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        subprocess.check_call(["umount", self.mount_point])
        os.rmdir(self.mount_point)
        os.remove(self.vbr)


class CreateAndMountPseudoDevice(object):
    def __init__(self, syslinux_path):
        self.syslinux_path = syslinux_path

    def __enter__(self):
        self.pseudo_device = self.create_pseudo_device()
        self.loop_device = self.associate_loop_device()
        self.create_filesystem()
        self.mount_point = self.mount()
        self.create_syslinux_dir()
        self.install_syslinux()
        self.vbr = utility.get_vbr(self.loop_device)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        utility.execute_command("umount", self.mount_point)
        self.detach_loop_device()
        os.rmdir(self.mount_point)
        os.remove(self.pseudo_device)
        os.remove(self.vbr)

    @staticmethod
    def create_pseudo_device():
        path = tempfile.mkstemp()[1]
        logging.debug("Creating pseudo device %r", path)
        # This seems to be the minimum size for both mkfs and the syslinux
        # installer to work (found experimentally)
        # It should actually be 65525 for FAT32, but syslinux fails with
        # "zero FAT sectors (FAT12/16)" if less than this
        file_size = 65525 + 1024 + 32
        utility.execute_command("dd", "if=/dev/zero", "of=%s" % path, "count=%s" % file_size)
        return path

    def associate_loop_device(self):
        logging.debug("Associating loop device with %r", self.pseudo_device)
        output, _ = utility.execute_command("losetup", "--find", "--show", self.pseudo_device)
        device = output.strip().decode()
        logging.debug("Associated device %r with %r", device, self.pseudo_device)
        return device

    def create_filesystem(self):
        utility.execute_command("mkfs", "-t", "msdos", "-F", "32", self.loop_device)

    def mount(self, path=None):
        if not path:
            path = tempfile.mkdtemp()
        utility.execute_command("mount", self.loop_device, path)
        return path

    def umount(self):
        utility.execute_command("umount", self.loop_device)

    def create_syslinux_dir(self):
        syslinux_dir = os.path.join(self.mount_point, "syslinux")
        logging.debug("Creating directory %r", syslinux_dir)
        os.mkdir(syslinux_dir)

    def install_syslinux(self):
        # The loop copy is used as a workaround for https://github.com/systemd/systemd/issues/540
        # See also crbug.com/508713 and chromium-review.googlesource.com/#/c/303962/1/update_bootloaders.sh
        logging.debug("Installing syslinux on %r", self.loop_device)
        self.umount()
        with tempfile.NamedTemporaryFile() as loop_copy:
            utility.execute_command("dd", "if=%s" % self.loop_device, "of=%s" % loop_copy.name)
            utility.execute_command(self.syslinux_path, "-d", "/syslinux/", loop_copy.name)
            utility.execute_command("flock", "--wait", "10", "-x", self.loop_device,
                                    "dd", "if=%s" % loop_copy.name, "of=%s" % self.loop_device)
        self.mount(self.mount_point)

    def detach_loop_device(self):
        utility.execute_command("losetup", "-d", self.loop_device)
