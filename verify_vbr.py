#!/usr/bin/env python3

import argparse
import logging
import os
import struct
import sys

import mount
import parse_ldlinux
import parse_vbr
import utility
from utility import assert_equal

usage = """Usage: {0} [-v] <iso> <partition>
Example: {0} ./iso/tails-i386-1.5.1.iso /dev/sdc1""".format(sys.argv[0])

PATH_TMP_NEW_VBR = "/tmp/vbr_new"
PATH_TMP_OLD_VBR = "/tmp/vbr_dev"


class VbrVerifier(object):
    def __init__(self, iso, pseudo_device, partition_path):
        logging.debug("Initializing VBR verifier")
        self.iso = iso
        self.pseudo_device = pseudo_device
        self.partition_path = partition_path
        tmp_ldlinux = os.path.join(self.pseudo_device.mount_point, "syslinux", "ldlinux.sys")
        self.epa = parse_ldlinux.get_epa(tmp_ldlinux)

    def __enter__(self):
        self.device_vbr = utility.get_vbr(self.partition_path)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.remove(self.device_vbr)

    def verify_vbr(self):
        self.patch_vbr()
        hash_tmp = utility.get_hash_sum(self.pseudo_device.vbr)
        hash_device = utility.get_hash_sum(self.device_vbr)
        if hash_tmp == hash_device:
            logging.info("VBR successfully verified")
            return True
        else:
            logging.warning("VBR verification failed")
            logging.info("- pseudo device")
            logging.info("+ Tails device")
            self.parse_files_and_print_diff(self.pseudo_device.vbr, self.device_vbr)
            return False

    def patch_vbr(self):
        self.patch_code()
        self.patch_geometry()

    def patch_code(self):
        self.patch_first_sector_pointer()

    def patch_first_sector_pointer(self):
        # TODO: See the comment regarding the LBA of the first sector of ldlinux.sys in parse_fat.py get_cluster_chain()
        #       TL;DR: The location of this LBA in the VBR is not necessarily fixed between different versions of
        #       syslinux, there might be better options to get this
        first_sector_pointer = parse_vbr.get_ldlinux_first_sector_lba(self.device_vbr)
        first_sector_pointer_string = struct.pack("<I", first_sector_pointer)
        with open(self.pseudo_device.vbr, 'r+b') as f:
            f.seek(self.epa["Sect1Ptr0Ptr"])
            logging.debug("Setting pointer to sect1 at %x to %r", self.epa['Sect1Ptr0Ptr'], first_sector_pointer_string)
            f.write(first_sector_pointer_string)
            f.seek(self.epa["Sect1Ptr1Ptr"])
            f.write(b"\0" * 4)

    def patch_geometry(self):
        # TODO: Maybe zero out values in both device_vbr and tmp_vbr instead of copying from device_vbr?
        sectors_per_cluster = 0x08
        hidden_sectors = 0x00000800
        huge_sectors = 0x0004e2000
        sectors_per_fat = 0x0000137f

        # sectors/track, heads, serial number and partition label depend on the device,
        # so we read them from the device's boot sector
        boot_sector = parse_vbr.get_fat32_boot_sector(self.device_vbr)
        logging.debug("sec_per_track: %r", boot_sector["SecPerTrack"])
        logging.debug("heads: %r", boot_sector["Heads"])
        logging.debug("serial_number: %r", boot_sector["VolumeID"])
        logging.debug("partition_label: %r", boot_sector["VolumeLabel"])
        logging.debug("reserved1: %r", boot_sector["Reserved1"])

        with open(self.pseudo_device.vbr, 'r+b') as f:
            f.seek(0xd)
            sectors_per_cluster_byte = struct.pack("<B", sectors_per_cluster)
            f.write(sectors_per_cluster_byte)

            f.seek(0x18)
            sec_per_track_word = struct.pack("<H", boot_sector["SecPerTrack"])
            f.write(sec_per_track_word)

            f.seek(0x1a)
            heads_word = struct.pack("<H", boot_sector["Heads"])
            f.write(heads_word)

            f.seek(0x1c)
            hidden_sectors_dword = struct.pack("<I", hidden_sectors)
            f.write(hidden_sectors_dword)

            f.seek(0x20)
            huge_sectorss_dword = struct.pack("<I", huge_sectors)
            f.write(huge_sectorss_dword)

            f.seek(0x24)
            sectors_per_fat_dword = struct.pack("<I", sectors_per_fat)
            f.write(sectors_per_fat_dword)

            f.seek(0x41)
            assert_equal(len(boot_sector["Reserved1"]), 1)
            f.write(boot_sector["Reserved1"])

            f.seek(0x43)
            serial_number_dword = struct.pack("<I", boot_sector["VolumeID"])
            f.write(serial_number_dword)

            assert_equal(len(boot_sector["VolumeLabel"]), 11)
            f.write(boot_sector["VolumeLabel"])

    def parse_files_and_print_diff(self, file1, file2):
        import difflib
        out1, out2 = self.parse_files(file1, file2)
        diff = difflib.ndiff(out1.read().splitlines(1), out2.read().splitlines(1))
        for line in ''.join(diff).splitlines():
            if line.startswith('+') or line.startswith('-'):  # or line.startswith('?'):
                logging.info(line)

    @staticmethod
    def parse_files(file1, file2):
        import io
        orig_stdout = sys.stdout
        out1 = io.StringIO()
        sys.stdout = out1
        parse_vbr.print_fat32_boot_sector(file1)
        out2 = io.StringIO()
        sys.stdout = out2
        parse_vbr.print_fat32_boot_sector(file2)
        sys.stdout = orig_stdout
        out1.seek(0)
        out2.seek(0)
        return out1, out2


def parse_args():
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument("--verbose", action="store_true")
    parser.add_argument("iso", help="Path to the Tails ISO image")
    parser.add_argument("partition", help="Path to the Tails partition to be verified")
    args = parser.parse_args()
    if not utility.is_block_device(args.partition):
        parser.error("%r doesn't seem to be a block device" % args.partition)
    if utility.is_sdx_drive_device(args.partition):
        parser.error("%r seems to be a drive instead of a partition" % args.partition)
    return args


def init(args):
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    logging.debug("args:", args)


def main():
    args = parse_args()
    init(args)

    with mount.Mount(args.iso) as iso, \
            mount.CreateAndMountPseudoDevice(iso.syslinux_path) as pseudo_device, \
            VbrVerifier(iso, pseudo_device, args.partition) as verifier:
        verifier.verify_vbr()


if __name__ == '__main__':
    main()
