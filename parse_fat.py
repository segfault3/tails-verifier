#!/usr/bin/env python3

import logging
import math
import os
import sys
import tempfile

import parse_vbr
import utility
from utility import assert_equal


usage = """Usage: {0} [-v] <partition> <num data sectors> <num adv sectors>
Example: {0} /dec/sdc1 118 2""".format(sys.argv[0])


class FatParser(object):
    def __init__(self, device):
        logging.debug("Initializing FAT32 parser")
        self.device = device
        self.device_name = os.path.basename(self.device)
        self._cluster_chain = None

    def __enter__(self):
        self.vbr = utility.get_vbr(self.device)
        self.boot_sector = parse_vbr.get_fat32_boot_sector(self.vbr)
        self.sectors_per_cluster = self.boot_sector["SecPerClust"]
        self.fat = self.get_fat()
        logging.debug("num_fats: 0x%x", self.boot_sector["FATs"])
        logging.debug("fat_size: 0x%x", self.boot_sector["FATSz32"])
        logging.debug("num_reserved_sectors: 0x%x", self.boot_sector["ResSectors"])
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.remove(self.vbr)
        os.remove(self.fat)

    def fat_offset_to_lba(self, fat_offset):
        # FAT = cluster map
        # => Each 4-Byte entry corresponds to a 8-Sector cluster
        # Needed: Offset of the cluster of sector 0x365970 in the FAT
        # sector - 2 * FAT - reserved sectors = 0x363250
        # cluster of the sector = 0x363250 / 8 + first_actual_cluster = 0x6c64a + 2 =  0x6c64c
        # offset of the cluster = 0x6c64c * 4 = 0x1b1930
        #
        # For a good explanation of LBA / Cluster number conversion see:
        #   http://www.unblockwebsite.net/unblock.php?u=RPu4B%2BpJn8jMsGV73aOiz8frkGm206I6m7Op4Kbo&b=13
        return self.cluster_number_to_lba(self.fat_offset_to_cluster(fat_offset))

    def lba_to_fat_offset(self, lba):
        return self.cluster_to_fat_offset(self.lba_to_cluster_number(lba))

    def cluster_number_to_lba(self, cluster):
        num_fats = self.boot_sector["FATs"]
        fat_size = self.boot_sector["FATSz32"]
        num_reserved_sectors = self.boot_sector["ResSectors"]
        first_data_sector = num_fats * fat_size + num_reserved_sectors
        sectors_per_cluster = self.boot_sector["SecPerClust"]
        first_cluster = self.boot_sector["RootClus"]
        return (cluster - first_cluster) * sectors_per_cluster + first_data_sector

    def lba_to_cluster_number(self, lba):
        num_fats = self.boot_sector["FATs"]
        fat_size = self.boot_sector["FATSz32"]
        num_reserved_sectors = self.boot_sector["ResSectors"]
        first_data_sector = num_fats * fat_size + num_reserved_sectors
        sectors_per_cluster = self.boot_sector["SecPerClust"]
        first_cluster = self.boot_sector["RootClus"]
        return (lba - first_data_sector) // sectors_per_cluster + first_cluster

    @staticmethod
    def cluster_to_fat_offset(cluster):
        return cluster * 4

    @staticmethod
    def fat_offset_to_cluster(fat_offset):
        return fat_offset // 4

    def get_fat(self):
        logging.debug("Extracting FAT")
        fat_path = tempfile.mkstemp()[1]
        sector_size = self.boot_sector["BytesPerSec"]
        reserved_sectors_count = self.boot_sector["ResSectors"]
        fat_size = self.boot_sector["FATSz32"]
        utility.execute_command("dd", "if=%s" % self.device, "of=%s" % fat_path,
                                "bs=%s" % sector_size, "skip=%s" % reserved_sectors_count, "count=%s" % fat_size)
        return fat_path

    def get_ldlinux_cluster_chain(self):
        if not self._cluster_chain:
            self._cluster_chain = self.extract_ldlinux_cluster_chain()
        return self._cluster_chain

    def extract_ldlinux_cluster_chain(self):
        logging.debug("Extracting cluster chain of ldlinux.sys from FAT")
        # TODO: Could a manipulated FAT exploit anything here?
        # It could cause an invalid file.seek() or file.read() obviously, but that doesn't seem dangerous to me
        # It could also make cluster_chain a big list of arbitrary content
        #   I don't like this, but a valid FAT could also contain pretty arbitrary content

        cluster_chain = list()
        # We need the LBA of the first sector of ldlinux.sys here, to look up the
        # complete cluster chain of ldlinux.sys
        # We have three possibilities to obtain the LBA:
        # 1: It is part of the code in the VBR, but it's location is not
        #    necessarily fixed between versions of syslinux
        # 2: Look ldlinux.sys up in the directory tables, which would require the
        #    code to read directory tables
        # 3: Use fibmap (avoiding fibmap was kind of the point of parsing the FAT
        #    in the first place)
        # TODO: For the time being, I go with option 1. But maybe 2 would be better
        first_sector_lba = parse_vbr.get_ldlinux_first_sector_lba(self.vbr)
        fat_offset = self.lba_to_fat_offset(first_sector_lba)
        logging.debug("fat_offset: %08x", fat_offset)
        logging.debug("first cluster: %08x", self.lba_to_cluster_number(first_sector_lba))
        cluster_chain.append(first_sector_lba)
        with open(self.fat, 'rb') as f:
            f.seek(fat_offset)
            next_cluster = parse_vbr.read(f, 4)[1]
            while next_cluster != 0x0FFFFFFF:
                logging.debug("next cluster: %08x lba: %08x", next_cluster, self.cluster_number_to_lba(next_cluster))
                cluster_chain.append(self.cluster_number_to_lba(next_cluster))
                f.seek(self.cluster_to_fat_offset(next_cluster))
                next_cluster = parse_vbr.read(f, 4)[1]
                # Ensure that a manipulated FAT can not make this an infinitely big list
                # 0x100000 clusters = 4 GB of data = max file size on FAT32 (with 8 Sectors/Cluster, 512 Bytes/Sector)
                assert len(cluster_chain) < 0x100000
        return cluster_chain

    # See syslinux/core/diskstart.inc l. 445 f.
    def get_ldlinux_extent_pointers(self, num_data_sectors, num_adv_sectors):
        # Walk the cluster chain of ldlinux.sys in the FAT to create the extent
        # pointers to the data sectors of ldlinux.sys

        cluster_chain = self.get_ldlinux_cluster_chain()
        # The sectors of ldlinux.sys don't necessarily fill up the last
        # cluster completely. But we can check if the size of the cluster chain
        # is in the right boundaries.
        num_sectors_in_clusters = len(cluster_chain) * self.sectors_per_cluster
        num_sectors_used = num_data_sectors + num_adv_sectors
        logging.debug("num sectors in clusters: %s", num_sectors_in_clusters)
        logging.debug("num sectors used: %s", num_sectors_used)
        assert_equal(num_sectors_in_clusters - self.sectors_per_cluster < num_sectors_used <= num_sectors_in_clusters)

        num_data_clusters = int(math.ceil(num_data_sectors / 8.0))
        data_clusters = cluster_chain[:num_data_clusters]
        extent_pointers = list()
        extent_pointers.append(data_clusters[0] + 1)
        sector_array_lengths = list()

        # Initialize the sector array's length with the size of a cluster,
        # because the sector arrays will have at least the length of one cluster
        # and will be incremented below.
        # Minus 1 because the first pointer actually points to the second sector
        # of ldlinux.sys
        sector_array_lengths.append(self.sectors_per_cluster - 1)

        previous_cluster = data_clusters[0]
        for cluster in data_clusters[1:]:
            if cluster != previous_cluster + self.sectors_per_cluster:
                self.add_new_extent_pointer(extent_pointers, cluster)
                logging.debug("adding extent pointer: %08x, len: 0x%x", extent_pointers[-1], sector_array_lengths[-1])
                sector_array_lengths.append(0)
            sector_array_lengths[-1] += self.sectors_per_cluster
            previous_cluster = cluster

        self.set_last_length(sector_array_lengths, num_data_sectors)

        return extent_pointers, sector_array_lengths

    def remove_last_cluster(self, extent_pointers, sector_array_lengths):
        sector_array_lengths[-1] -= self.sectors_per_cluster
        if sector_array_lengths[-1] <= 0:
            extent_pointers.pop()
            sector_array_lengths.pop()

    def last_cluster_contains_data_sectors(self, num_data_sectors, num_adv_sectors):
        # The ADV sectors of ldlinux.sys are appended to the data sectors. If
        # they cross a cluster's boundary, there will be an additional cluster
        # which does not contain data sectors, so we must exclude it.
        # Note that we assume that the ADV sectors can not fill up more tha"n
        # one cluster (in current syslinux there are 2 ADV sectors)
        assert_equal(num_adv_sectors < self.sectors_per_cluster)
        logging.debug("num_data_sectors: %s", num_data_sectors)
        logging.debug("num_data_sectors \% sectors_per_cluster: %s", num_data_sectors % self.sectors_per_cluster)
        logging.debug("num_adv_sectors: %s", num_adv_sectors)
        num_sectors_free_in_last_cluster = self.sectors_per_cluster - (num_data_sectors % self.sectors_per_cluster)
        last_cluster_contains_only_adv_sectors = num_sectors_free_in_last_cluster < num_adv_sectors
        if last_cluster_contains_only_adv_sectors:
            logging.debug("Last cluster contains only ADV sectors. Removing it from the extent pointers.")
        return not last_cluster_contains_only_adv_sectors

    @staticmethod
    def set_last_length(lengths, num_data_sectors):
        # Minus 1 because the first pointer actually points to the second sector
        # of ldlinux.sys
        num_sectors = num_data_sectors - 1
        # Set the length of the last sector array to the rest of the data sectors
        logging.debug("Setting length of last sector array to 0x%x", num_sectors - sum(lengths[:-1]))
        lengths[-1] = num_sectors - sum(lengths[:-1])

    @staticmethod
    def add_new_extent_pointer(sector_pointers, cluster):
        sector_pointers.append(cluster)

    def get_ldlinux_adv_sector_pointers(self, num_data_sectors, num_adv_sectors):
        if num_adv_sectors <= 0:
            return list()
        adv_sector_pointers = list()
        cluster_chain = self.get_ldlinux_cluster_chain()
        num_data_clusters = int(math.ceil(num_data_sectors / 8.0))
        free_sectors_in_last_data_cluster = self.sectors_per_cluster - (num_data_sectors % self.sectors_per_cluster)
        last_cluster_lba = cluster_chain[num_data_clusters - 1]
        for i in range(free_sectors_in_last_data_cluster):
            lba = last_cluster_lba + self.sectors_per_cluster - free_sectors_in_last_data_cluster + i
            adv_sector_pointers.append(lba)
            if len(adv_sector_pointers) == num_adv_sectors:
                return adv_sector_pointers
        adv_clusters = cluster_chain[num_data_clusters:]
        for cluster in adv_clusters:
            for i in range(self.sectors_per_cluster):
                lba = cluster + i
                adv_sector_pointers.append(lba)
                if len(adv_sector_pointers) == num_adv_sectors:
                    return adv_sector_pointers

        raise RuntimeError(
            "Cluster chain of ldlinux.sys doesn't match number of data sectors (%r) "
            "and number of ADV sectors (%r)" % num_data_sectors, num_adv_sectors)


def parse_args():
    if "-v" in sys.argv:
        logging.basicConfig(level=logging.DEBUG)
        del (sys.argv[sys.argv.index("-v")])
    else:
        logging.basicConfig(level=logging.INFO)

    if len(sys.argv) < 4:
        sys.exit(usage)

    return sys.argv[1], int(sys.argv[2]), int(sys.argv[3])


def main():
    device, num_data_sectors, num_adv_sectors = parse_args()
    with FatParser(device) as parser:
        extent_pointers, sector_array_lengths = parser.get_ldlinux_extent_pointers(num_data_sectors, num_adv_sectors)

    print("Extent pointers:")
    for i, pointer in enumerate(extent_pointers):
        print("pointer: %08x, len: %x" % (pointer, sector_array_lengths[i]))


if __name__ == "__main__":
    main()
