#!/usr/bin/env python3

import unittest
import os
import sys
import glob

script_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.abspath(os.path.join(script_dir, ".."))
sys.path.append(parent_dir)

import utility

verify_device_script = os.path.join(parent_dir, "verify_device.py")
verify_files_script = os.path.join(parent_dir, "verify_files.py")
verify_mbr_script = os.path.join(parent_dir, "verify_mbr.py")
verify_vbr_script = os.path.join(parent_dir, "verify_vbr.py")
verify_ldlinux_script = os.path.join(parent_dir, "verify_ldlinux.py")

device_image_dir = os.path.join(parent_dir, "device_images")
hashes_dir = os.path.join(parent_dir, "hashes")


class TestVerification(object):
    device_image_name = None

    @classmethod
    def setUpClass(cls):
        device_image_path = os.path.join(device_image_dir, cls.device_image_name)
        out, _ = utility.execute_command("losetup", "-f", "--show", "-P", device_image_path)
        cls.loop_device = out.decode().strip()
        cls.partition = cls.loop_device + "p1"
        cls.iso_path = utility.get_iso_file_path(cls.partition)

    @classmethod
    def tearDownClass(cls):
        utility.execute_command("losetup", "-d", cls.loop_device)

    def test_verify_device(self):
        utility.execute_command(verify_device_script, self.loop_device)

    def test_verify_files(self):
        utility.execute_command(verify_files_script, self.partition)

    def test_verify_mbr(self):
        utility.execute_command(verify_mbr_script, self.iso_path, self.loop_device)

    def test_verify_vbr(self):
        utility.execute_command(verify_vbr_script, self.iso_path, self.partition)

    def test_verify_ldlinux(self):
        utility.execute_command(verify_ldlinux_script, self.iso_path, self.partition)


class TestVerification1(TestVerification, unittest.TestCase):
    device_image_name = "tails_i386-2.0~beta1.img"


class TestVerification2(TestVerification, unittest.TestCase):
    device_image_name = "tails_i386-2.0~beta1_iuk_upgraded_to_2.0~rc1.img"


class TestVerification3(TestVerification, unittest.TestCase):
    device_image_name = "tails_i386-1.5.1_iuk_upgraded_to_1.8.2.img"


class TestVerification4(TestVerification, unittest.TestCase):
    device_image_name = "tails_i386-2.0.img"


if __name__ == '__main__':
    hashes = glob.glob(hashes_dir + "/*")
    for f in hashes:
        os.remove(f)
    unittest.main()